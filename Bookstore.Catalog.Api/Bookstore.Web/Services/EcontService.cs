﻿using Bookstore.Web.EcontApi;
using Bookstore.Web.Econt;
using Bookstore.Web.OpenLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Label = Bookstore.Web.EcontApi.Label;

namespace Bookstore.Web.Services
{
    public class EcontService
    {

        public HttpClient Client { get; set; }
        public object SenderClient { get; private set; }

        public EcontService(HttpClient client)
        {
            Client = client;
        }


        public async Task<LabelResponse> CreateLabel()
        {
            var label = new LabelRequest()
            {
                Mode = "create",
                Label = new Label()
                {
                    SenderClient = new ErClient()
                    {
                        Name = "Иван Иванов",
                        Phones = new List<string>() { "0888888888" },
                    },

                    SenderAddress = new ErAddress()
                    {
                        City = new City()
                        {
                            Country = new Country()
                            {
                                Code3 = "BGR"
                            },
                            Name = "София",
                            PostCode = 1700
                        },
                        Street = "Алея Младост",
                        Num = 7

                    },
                    ReceiverClient = new ErClient()
                    {
                        Name = "Димитър Димитров",
                        Phones = new List<string>() { "0876543210" }
                    },
                    ReceiverAddress = new ErAddress()
                    {

                        City = new City()
                        {
                            Country = new Country()
                            {

                                Code3 = "BGR",

                            },
                            Name = "София",
                            PostCode = 1000


                        },
                        Street = "Муткурова",
                        Num = 84
                    },

                    PackCount = 1,
                    ShipmentType = "PACK",
                    ShipmentDescription = "Книги",
                    Weight = 1

                }
            };



            var content = new StringContent(JsonConvert.SerializeObject(label), Encoding.UTF8, "application/json");
            var authString = Convert.ToBase64String(Encoding.ASCII.GetBytes("iasp-dev:iasp-dev"));
            Client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", authString);
            var response = await Client.PostAsync("Shipments/LabelService.createLabel.json", content);

            var labelResponse = await response.Content.ReadAsStringAsync();
            var econtResponse = JsonConvert.DeserializeObject<LabelResponse>(labelResponse);
            return econtResponse;
        }
    }
}
