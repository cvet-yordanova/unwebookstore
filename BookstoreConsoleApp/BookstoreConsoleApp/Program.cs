﻿using BooksApi;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BookstoreConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            using(var client = new HttpClient())
            {
                var result = await client.GetStringAsync("https://localhost:44307/api/books");
                var allBooks = JsonSerializer.Deserialize<List<Book>>(result, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            
                 foreach(var b in allBooks)
                {
                    Console.WriteLine(b.Title);
                }


                var api = new Bookstore("https://localhost:44307", client);
                var books = await api.GetAllAsync();
                foreach (var b in books)
                {
                    Console.WriteLine(b.Title);
                }

            }
        }


    }

    public class Book
    {
        public int BookID { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int PublisherID { get; set; }
        public int LanguageID { get; set; }
        public decimal Price { get; set; }
        public string LanguageName { get; set; }
        public string PublisherCompanyName { get; set; }
    }
}
