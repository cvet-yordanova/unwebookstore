﻿using System;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Org.OpenAPITools.Api;
namespace ConsoleApp1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            var api = new BooksApi("https://localhost:44307");
            var books = await api.GetAllAsync();

            foreach (var book in books)
            {
                Console.WriteLine(book.Title);
            }

        }
    }
}
