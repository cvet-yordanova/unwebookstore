# Org.OpenAPITools - the C# library for the Bookstore Catalog API

An api for the bookstore catalog

This C# SDK is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: v1
- SDK version: 1.0.0
- Build package: org.openapitools.codegen.languages.CSharpNetCoreClientCodegen

<a name="frameworks-supported"></a>
## Frameworks supported
- .NET Core >=1.0
- .NET Framework >=4.6
- Mono/Xamarin >=vNext

<a name="dependencies"></a>
## Dependencies

- [RestSharp](https://www.nuget.org/packages/RestSharp) - 106.13.0 or later
- [Json.NET](https://www.nuget.org/packages/Newtonsoft.Json/) - 12.0.3 or later
- [JsonSubTypes](https://www.nuget.org/packages/JsonSubTypes/) - 1.8.0 or later
- [System.ComponentModel.Annotations](https://www.nuget.org/packages/System.ComponentModel.Annotations) - 5.0.0 or later

The DLLs included in the package may not be the latest version. We recommend using [NuGet](https://docs.nuget.org/consume/installing-nuget) to obtain the latest version of the packages:
```
Install-Package RestSharp
Install-Package Newtonsoft.Json
Install-Package JsonSubTypes
Install-Package System.ComponentModel.Annotations
```

NOTE: RestSharp versions greater than 105.1.0 have a bug which causes file uploads to fail. See [RestSharp#742](https://github.com/restsharp/RestSharp/issues/742).
NOTE: RestSharp for .Net Core creates a new socket for each api call, which can lead to a socket exhaustion problem. See [RestSharp#1406](https://github.com/restsharp/RestSharp/issues/1406).

<a name="installation"></a>
## Installation
Generate the DLL using your preferred tool (e.g. `dotnet build`)

Then include the DLL (under the `bin` folder) in the C# project, and use the namespaces:
```csharp
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;
```
<a name="usage"></a>
## Usage

To use the API client with a HTTP proxy, setup a `System.Net.WebProxy`
```csharp
Configuration c = new Configuration();
System.Net.WebProxy webProxy = new System.Net.WebProxy("http://myProxyUrl:80/");
webProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
c.Proxy = webProxy;
```

<a name="getting-started"></a>
## Getting Started

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

namespace Example
{
    public class Example
    {
        public static void Main()
        {

            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new BooksApi(config);
            var bookRequest = new BookRequest(); // BookRequest |  (optional) 

            try
            {
                Book result = apiInstance.Create(bookRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling BooksApi.Create: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }

        }
    }
}
```

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BooksApi* | [**Create**](docs/BooksApi.md#create) | **POST** /api/Books | 
*BooksApi* | [**Delete**](docs/BooksApi.md#delete) | **DELETE** /api/Books/{bookId} | 
*BooksApi* | [**GetAll**](docs/BooksApi.md#getall) | **GET** /api/Books | 
*BooksApi* | [**GetById**](docs/BooksApi.md#getbyid) | **GET** /api/Books/{id} | 
*BooksApi* | [**Update**](docs/BooksApi.md#update) | **PUT** /api/Books/{bookID} | 
*BooksApi* | [**Update_0**](docs/BooksApi.md#update_0) | **PATCH** /api/Books/{bookId} | 
*WeatherForecastApi* | [**WeatherForecastGet**](docs/WeatherForecastApi.md#weatherforecastget) | **GET** /WeatherForecast | 


<a name="documentation-for-models"></a>
## Documentation for Models

 - [Model.Author](docs/Author.md)
 - [Model.Book](docs/Book.md)
 - [Model.BookAuthor](docs/BookAuthor.md)
 - [Model.BookAuthorResponse](docs/BookAuthorResponse.md)
 - [Model.BookGenre](docs/BookGenre.md)
 - [Model.BookGenreResponse](docs/BookGenreResponse.md)
 - [Model.BookRequest](docs/BookRequest.md)
 - [Model.BookRequestJsonPatchDocument](docs/BookRequestJsonPatchDocument.md)
 - [Model.BookRequestOperation](docs/BookRequestOperation.md)
 - [Model.BookResponse](docs/BookResponse.md)
 - [Model.Genre](docs/Genre.md)
 - [Model.Language](docs/Language.md)
 - [Model.OperationType](docs/OperationType.md)
 - [Model.ProblemDetails](docs/ProblemDetails.md)
 - [Model.Publisher](docs/Publisher.md)
 - [Model.WeatherForecast](docs/WeatherForecast.md)


<a name="documentation-for-authorization"></a>
## Documentation for Authorization

All endpoints do not require authorization.
