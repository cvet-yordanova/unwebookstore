# Org.OpenAPITools.Model.BookAuthorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AuthorID** | **int** |  | [optional] 
**AuthorFirstName** | **string** |  | [optional] 
**AuthorLastName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

