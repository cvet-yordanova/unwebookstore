# Org.OpenAPITools.Model.BookRequestJsonPatchDocument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Operations** | [**List&lt;BookRequestOperation&gt;**](BookRequestOperation.md) |  | [optional] [readonly] 
**ContractResolver** | **Object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

