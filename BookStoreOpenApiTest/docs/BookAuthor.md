# Org.OpenAPITools.Model.BookAuthor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BookID** | **int** |  | [optional] 
**AuthorID** | **int** |  | [optional] 
**Position** | **int** |  | [optional] 
**Book** | [**Book**](Book.md) |  | [optional] 
**Author** | [**Author**](Author.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

