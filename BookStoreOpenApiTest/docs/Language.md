# Org.OpenAPITools.Model.Language

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LanguageID** | **int** |  | [optional] 
**Name** | **string** |  | [optional] 
**Books** | [**List&lt;Book&gt;**](Book.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

