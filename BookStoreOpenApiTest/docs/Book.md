# Org.OpenAPITools.Model.Book

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BookID** | **int** |  | [optional] 
**Isbn** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Cover** | **byte[]** |  | [optional] 
**Year** | **int** |  | [optional] 
**PublisherID** | **int** |  | [optional] 
**LanguageID** | **int** |  | [optional] 
**Price** | **double** |  | [optional] 
**CreatedOn** | **DateTime** |  | [optional] 
**ModifiedOn** | **DateTime** |  | [optional] 
**Language** | [**Language**](Language.md) |  | [optional] 
**Publisher** | [**Publisher**](Publisher.md) |  | [optional] 
**Authors** | [**List&lt;BookAuthor&gt;**](BookAuthor.md) |  | [optional] 
**Genres** | [**List&lt;BookGenre&gt;**](BookGenre.md) |  | [optional] 
**Author** | [**Author**](Author.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

