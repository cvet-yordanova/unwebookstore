# Org.OpenAPITools.Model.BookRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Isbn** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 
**Year** | **int?** |  | [optional] 
**PublisherID** | **int?** |  | [optional] 
**LanguageID** | **int?** |  | [optional] 
**Price** | **double?** |  | [optional] 
**Authors** | **List&lt;int&gt;** |  | [optional] 
**Genres** | **List&lt;int&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

