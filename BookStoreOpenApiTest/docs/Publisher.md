# Org.OpenAPITools.Model.Publisher

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PublisherID** | **int** |  | [optional] 
**CompanyName** | **string** |  | [optional] 
**Country** | **string** |  | [optional] 
**Website** | **string** |  | [optional] 
**Books** | [**List&lt;Book&gt;**](Book.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

