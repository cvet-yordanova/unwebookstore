# Org.OpenAPITools.Model.Genre

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GenreID** | **int** |  | [optional] 
**Name** | **string** |  | [optional] 
**Books** | [**List&lt;BookGenre&gt;**](BookGenre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

