# Org.OpenAPITools.Model.BookGenreResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GenreID** | **int** |  | [optional] 
**GenreName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

