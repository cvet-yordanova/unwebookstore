# Org.OpenAPITools.Model.Author

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AuthorID** | **int** |  | [optional] 
**FirstName** | **string** |  | [optional] 
**LastName** | **string** |  | [optional] 
**About** | **string** |  | [optional] 
**Photo** | **byte[]** |  | [optional] 
**BirthYear** | **string** |  | [optional] 
**Nationality** | **string** |  | [optional] 
**Books** | [**List&lt;BookAuthor&gt;**](BookAuthor.md) |  | [optional] 
**CreatedOn** | **DateTime** |  | [optional] 
**ModifiedOn** | **DateTime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

