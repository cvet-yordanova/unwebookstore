# Org.OpenAPITools.Model.BookGenre

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BookID** | **int** |  | [optional] 
**GenreID** | **int** |  | [optional] 
**Book** | [**Book**](Book.md) |  | [optional] 
**Genre** | [**Genre**](Genre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

