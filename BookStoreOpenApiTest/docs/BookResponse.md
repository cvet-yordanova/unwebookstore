# Org.OpenAPITools.Model.BookResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BookID** | **int** |  | [optional] 
**Isbn** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 
**Year** | **int** |  | [optional] 
**PublisherID** | **int** |  | [optional] 
**LanguageID** | **int** |  | [optional] 
**Price** | **double** |  | [optional] 
**LanguageName** | **string** |  | [optional] 
**PublisherCompanyName** | **string** |  | [optional] 
**Authors** | [**List&lt;BookAuthorResponse&gt;**](BookAuthorResponse.md) |  | [optional] 
**Genres** | [**List&lt;BookGenreResponse&gt;**](BookGenreResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

